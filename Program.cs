﻿using System.Threading.Tasks;

namespace RSA
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var encoded = await RSAEncoder.EncodeFileTextAsync("data.txt");
            await RSAEncoder.DecodeAndWriteToFileAsync(encoded, "data-out.txt");
        }
    }
}