﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace RSA
{
    public static class RSAEncoder
    {
        private static RsaConfiguration _configuration;
        private static void Init()
        {
            if (_configuration == null)
            {
                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                _configuration = BuildConfiguration();
            }
        }

        public static async Task<int[]> EncodeFileTextAsync(string filePath)
        {
            var message = await File.ReadAllTextAsync(Path.Combine(AppContext.BaseDirectory, filePath));
            return await EncodeTextAsync(message);
        }

        public static Task<int[]> EncodeTextAsync(string text)
        {
            var watch = new Stopwatch();
            watch.Start();
            Init();
            var origBytes = Encoding.UTF8.GetBytes(text);
            var encryptedData = new List<int>();
            foreach (var code in origBytes)
            {
                var m = (int)code;
                var mc = BigInteger.ModPow(m, (BigInteger)_configuration.E, (BigInteger)_configuration.N);
                encryptedData.Add((int)mc);
            }
            watch.Stop();
            Console.WriteLine("Encoded in: " + watch.ElapsedMilliseconds + "ms");
            return Task.FromResult(encryptedData.ToArray());
        }

        public static async Task DecodeAndWriteToFileAsync(int[] encoded, string filePath)
        {
            var decryptedBytes = new List<byte>();
            var watch = new Stopwatch();
            watch.Start();
            Init();
            foreach (var code in encoded)
            {
                var m = Convert.ToInt32(code);
                var mo = BigInteger.ModPow(m, (BigInteger)_configuration.D, (BigInteger)_configuration.N);
                decryptedBytes.Add((byte)(int)mo);
            }
            watch.Stop();
            Console.WriteLine("Decoded in: " + watch.ElapsedMilliseconds + "ms");
            await File.WriteAllBytesAsync(Path.Combine(AppContext.BaseDirectory, filePath), decryptedBytes.ToArray());
        }

        #region Helpers

        private static RsaConfiguration BuildConfiguration()
        {
            double p, q;
            do
            {
                p = GetRandomPrimeNumber();
                q = GetRandomPrimeNumber();
            } while (p + q < 255);

            Console.WriteLine($"p = {p}, q = {q}");

            var n = p * q;

            Console.WriteLine($"n = {n}");

            double fi = (p - 1) * (q - 1);

            Console.WriteLine($"f(n) = {fi}");

            double e = GetEpsilon(p, q);
            Console.WriteLine($"e = {e}");

            double k = 1;
            double d = 1.1;

            Console.WriteLine("Start to calculate d");
            while (d - (int)d > 0)
            {
                Console.Write($"d = (1 + {k} * {fi}) / {e}");
                d = (float)(1 + k++ * fi) / e;
                Console.Write(" = " + d + " , k = " + (k - 1));
                Console.WriteLine();
            }

            Console.WriteLine($"\n d = {d}, k = {k}\n");

            Console.WriteLine($"Cpub = ({e},{n})");
            Console.WriteLine($"Cpriv = ({d},{n})");

            return new RsaConfiguration
            {
                P = p,
                Q = q,
                D = d,
                Fi = fi,
                N = n,
                E = e
            };
        }

        public static double GetEpsilon(double p, double q)
        {
            double e;
            do
            {
                e = GetRandomPrimeNumber();
            } while (e % p == 0 || e % q == 0);
            return e;
        }

        private static double GetRandomPrimeNumber()
        {
            var random = new Random();
            var number = 0;
            while (!IsPrime(number))
            {
                number = random.Next(1, 255);
            }

            return number;
        }

        private static bool IsPrime(int number)
        {
            if (number <= 1) return false;
            if (number == 2) return true;
            if (number % 2 == 0) return false;

            var boundary = (int)Math.Floor(Math.Sqrt(number));

            for (int i = 3; i <= boundary; i += 2)
                if (number % i == 0)
                    return false;

            return true;
        }

        #endregion
        private class RsaConfiguration
        {
            public double P { get; set; }
            public double Q { get; set; }
            public double N { get; set; }
            public double Fi { get; set; }
            public double D { get; set; }
            public double E { get; set; }
        }
    }
}